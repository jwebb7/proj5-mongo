"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)
"""
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
import os

###
# Globals
###
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
to_db = []

###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")

    return render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    return render_template('404.html'), 404

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    global to_db
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', type=float)
    date = request.args.get('open_date', type=str)
    time = request.args.get('open_time', type=str)
    index = request.args.get('index', type=int)

    date_time = date + ' ' + time
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    time = arrow.get(date_time, 'YYYY-MM-DD HH:mm').isoformat()
    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)
    result = {"open": open_time, "close": close_time}
    db_entry = {"index": index, "km": km, "distance": distance, "date": date,
                "time": time, "open": open_time, "close": close_time}
    for line in to_db:
        if db_entry['index'] == line['index']:
            to_db.remove(line)
    to_db.append(db_entry)
    return flask.jsonify(result=result)

@app.route("/submit")
def submit():
    global to_db
    result = {"valid": True}
    if len(to_db) == 0:
        result['valid'] = False
    else:
        for line in to_db:
                db.tododb.insert_one(line)
        to_db = []
    return flask.jsonify(result=result)

@app.route('/display')
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    if len(items) == 0:
            return render_template('empty.html')
    return render_template('display.html', items=items)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
